#include <text/ft_class.h>
#include <stdexcept>

#include <iostream>
namespace text{

	freetype_lib::freetype_lib(){
		if ( FT_Init_FreeType(&library) )
		{
			throw std::runtime_error("freetype library failed to initialise"); 
		}

		if (FT_New_Face(library, "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf", 0, &face)){
			throw std::runtime_error("cannot find font"); 

		}
		FT_Library_Version(library, &major, &minor, &patch);
		std::cout<<"initialised freetype library with version: "<<major<<"."<<minor<<"."<<patch<<std::endl;



		FT_Set_Pixel_Sizes(face, 0, 24); 
	}


	freetype_lib::~freetype_lib(){
		// FT_Done_Face(face);
		// FT_Done_FreeType(library);
	}

	std::vector<int> freetype_lib::version() const{
		return std::vector<int>{major, minor, patch};
	}

	bool freetype_lib::compare_version(const int& _major, const int& _minor, const int& _patch) noexcept{
		if(_major == major){
			if(_minor == minor){
				if(_patch == patch){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	FT_Library freetype_lib::get_library(){
		return library;
	}

	freetype_lib freetype_lib::get_current_object(){
		return *this;
	}
	FT_Face freetype_lib::get_face(){
		return face;
	}

}