#include <shading/shader.h>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
shader::shader(){
	GLenum err;
	shaderProgram = glCreateProgram();
	std::cout<<"shader program created"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}
	glAttachShader(shaderProgram, fs.program());
	std::cout<<"attached fragment shader"<<std::endl;

	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}
	glAttachShader(shaderProgram, vs.program());
	std::cout<<"attached vertex shader"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}
	
	glLinkProgram(shaderProgram); 
	std::cout<<"linked"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}

	glUseProgram(shaderProgram);
	std::cout<<"using program"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}
	std::cout<<std::endl;
	GLint out;
	glGetProgramiv(shaderProgram,  GL_VALIDATE_STATUS, &out);
	std::cout<<"validate status: "<<out<<std::endl;

	glGetProgramiv(shaderProgram,  GL_LINK_STATUS, &out);
	std::cout<<"link status: "<<out<<std::endl;

	glGetProgramiv(shaderProgram,  GL_ATTACHED_SHADERS, &out);
	std::cout<<"number of attached shaders: "<<out<<std::endl;


	glGetProgramiv(shaderProgram,  GL_ACTIVE_UNIFORMS, &out);
	std::cout<<"number of uniforms: "<<out<<std::endl;

	glGetProgramiv(shaderProgram,  GL_PROGRAM_BINARY_LENGTH, &out);
	std::cout<<"program size: "<<out<<std::endl;

	std::cout<<"shader end"<<std::endl;
	std::cout<<std::endl;

}

shader::shader(enum shader::type t){
	
	if(t == text){
		fs = fs.construct_text_shader();
	}else if(t == button){
		fs = fs.construct_button_shader();
	}

	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vs.program());
	glAttachShader(shaderProgram, fs.program());
	glLinkProgram(shaderProgram); 
	glUseProgram(shaderProgram);
}

const void shader::updateUniform4f(std::string uniform_name, const std::vector<float>& uniform_data) const{
	
	GLenum err;
	glUseProgram(shaderProgram);

	int index = glGetUniformLocation(shaderProgram, uniform_name.c_str());
	std::cout<<"shader index is: "<<index<<std::endl;
	glValidateProgram(shaderProgram);
	

	std::cout<<"trying to update uniform <"<<uniform_name<<"> at index: "<<index<<std::endl;
	glUniform4f(index, uniform_data[0], uniform_data[1], uniform_data[2], uniform_data[3]);
	
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<err<<std::endl;
		std::cout<<gluErrorString(err)<<std::endl;
	}

}
const void shader::updateUniform4mat(std::string uniform_name, glm::mat4 matrix) const{
	
	GLenum err;
	glUseProgram(shaderProgram);

	int index = glGetUniformLocation(shaderProgram, uniform_name.c_str());
	// std::cout<<"shader index is: "<<index<<std::endl;
	glValidateProgram(shaderProgram);
	std::cout<<"trying to update uniform <"<<uniform_name<<"> at index: "<<index<<std::endl;
	// std::cout<<"trying to update uniform at index: "<<index<<std::endl;
	glUniformMatrix4fv(index, 1, false, glm::value_ptr(matrix));
	
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<err<<std::endl;
		std::cout<<gluErrorString(err)<<std::endl;
	}

}


const void shader::updateUniform3f(std::string uniform_name, const std::vector<float>& uniform_data) const{
	

	GLenum err;
	glUseProgram(shaderProgram);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	int index = glGetUniformLocation(shaderProgram, uniform_name.c_str());
	// std::cout<<"shader index is: "<<index<<std::endl;
	glValidateProgram(shaderProgram);
	
	// return glGetUniformBlockIndex(shaderProgram, uniform_name);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	// std::cout<<"trying to update uniform at index: "<<index<<std::endl;
	std::cout<<"trying to update uniform <"<<uniform_name<<"> at index: "<<index<<std::endl;
// 
	glUniform3f(index, uniform_data[0], uniform_data[1], uniform_data[2]);
	
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}

}

const void shader::updateUniform1i(std::string uniform_name, int data) const{
	

	GLenum err;
	glUseProgram(shaderProgram);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	int index = glGetUniformLocation(shaderProgram, uniform_name.c_str());
	std::cout<<"shader index is: "<<index<<std::endl;
	glValidateProgram(shaderProgram);
	
	// return glGetUniformBlockIndex(shaderProgram, uniform_name);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	// std::cout<<"trying to update uniform at index: "<<index<<std::endl;
	std::cout<<"trying to update uniform <"<<uniform_name<<"> at index: "<<index<<std::endl;

	glUniform1i(index, data);
	
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}

}

const void shader::bindLocation(std::string attribute, int location) const{
	

	GLenum err;
	glUseProgram(shaderProgram);
	
	std::cout<<"trying to bind attribute at location: "<<location<<std::endl;

	glBindAttribLocation(shaderProgram ,location, attribute.c_str());
	
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}

}

const void shader::useProgram() const{
	glUseProgram(shaderProgram);
}

const unsigned int shader::getProgram() const{
	return shaderProgram;
}