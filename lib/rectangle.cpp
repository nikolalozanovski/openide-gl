#include <primitives/rectangle.h>

namespace primitives{
	namespace p2d{
		rect2d::rect2d(const vertex& v1, const vertex& v2, const vertex& v3,const vertex& v4){
			data.reserve(12);
			data.emplace_back(std::move(v1.getX()));
			data.emplace_back(std::move(v1.getY()));
			data.emplace_back(std::move(0.0));

			data.emplace_back(std::move(v2.getX()));
			data.emplace_back(std::move(v2.getY()));
			data.emplace_back(std::move(0.0));

			data.emplace_back(std::move(v3.getX()));
			data.emplace_back(std::move(v3.getY()));
			data.emplace_back(std::move(0.0));

			data.emplace_back(std::move(v4.getX()));
			data.emplace_back(std::move(v4.getY()));
			data.emplace_back(std::move(0.0));

		}

		rect2d::rect2d(const line2d& l1,const line2d& l2, const line2d& l3, const line2d& l4){
			data.reserve(12);
			auto t = l1.this_vertices();
			data.emplace_back(t[0].getX());
			data.emplace_back(t[0].getY());
			data.emplace_back(0.0);

			data.emplace_back(t[1].getX());
			data.emplace_back(t[1].getY());
			data.emplace_back(0.0);
			
			t = l2.this_vertices();
			data.emplace_back(t[1].getX());
			data.emplace_back(t[1].getY());
			data.emplace_back(0.0);

			t = l3.this_vertices();
			data.emplace_back(t[1].getX());
			data.emplace_back(t[1].getY());
			data.emplace_back(0.0);
		}


		rect2d::rect2d(const line2d& l1,const line2d& l2){
			auto t = l1.this_vertices();
			data.emplace_back(t[0].getX());
			data.emplace_back(t[0].getY());
			data.emplace_back(0.0);
			data.emplace_back(t[1].getX());
			data.emplace_back(t[1].getY());
			data.emplace_back(0.0);
			t = l2.this_vertices();
			data.emplace_back(t[0].getX());
			data.emplace_back(t[0].getY());
			data.emplace_back(0.0);
			data.emplace_back(t[1].getX());
			data.emplace_back(t[1].getY());
			data.emplace_back(0.0);
		}

		const std::string rect2d::getType(){
			return "2DRECTANGLE";
		}
	}
}