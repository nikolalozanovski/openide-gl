#include <primitives/line.h>

namespace primitives{
	namespace p2d{

		line2d::line2d(const vertex& v1,const vertex& v2){
			data.reserve(6);
			data.emplace_back(std::move(v1.getX()));
			data.emplace_back(std::move(v1.getY()));
			data.emplace_back(std::move(v1.getZ()));

			data.emplace_back(std::move(v2.getX()));
			data.emplace_back(std::move(v2.getY()));
			data.emplace_back(std::move(v2.getZ()));
		}
		line2d::line2d(float x1, float y1, float x2, float y2){
			data.reserve(6);

			data.emplace_back(x1);
			data.emplace_back(y1);
			data.emplace_back(0.0);
			
			data.emplace_back(x2);
			data.emplace_back(y2);
			data.emplace_back(0.0);

		}
		const std::string line2d::getType(){
			return "2DLINE";
		}

		std::vector<vertex> line2d::this_vertices() const{
			std::vector<vertex> vec{vertex(data[0], data[1], data[2]), vertex(data[3], data[4], data[5])};
			return vec;
		}

	}
}