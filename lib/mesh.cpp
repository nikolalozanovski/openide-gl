#include <render/mesh.h>
#ifndef BUFF_OFFSET
#define BUFF_OFFSET
#define BUFFER_OFFSET(bytes)((GLubyte*) NULL + (bytes))
#endif
#include <application.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
namespace render{


	const unsigned int  indices[][3] = {

	// {0,1},
	// {1,2},
	// {2,3},
	// {3,0},

		{0,1,3},
		{1,2,3},
		{0,1},
		{1,2},
		{2,3},
		{3,0}
	};




	const void mesh::prepare(const std::vector<float>& VertexBuff, const std::vector<float>& color_inside, const std::vector<float>& color_border){
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vertexBuff);
		glGenBuffers(1, &indicesBuff);
		

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuff);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)* sizeof(VertexBuff), &VertexBuff[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glEnableClientState(GL_VERTEX_ARRAY);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuff);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(int)* sizeof(&indices), &indices, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, 0);


	// sh.updateUniform("aPos", {0.5, 0.5, 0.0, 1});
	// sh.updateUniform("color", {0.2, 0.2, 0.2, 1});
		// sh.updateUniform4f("color", color_inside);
		// sh2.updateUniform4f("color", color_border);
	// sh3.updateUniform("aPos", VertexBuff);
	// sh2.updateUniform("color", {0.1, 0.5, 0.7, 1});

		glm::mat4 projection = glm::ortho(0.0f, (float)Application::get_width(), 0.0f, (float)Application::get_height(), -1.0f, 1.0f);
	// sh.updateUniform("aPos", {0.5, 0.5, 0.0, 1});
	// sh.updateUniform("color", {0.2, 0.2, 0.2, 1});
		sh.updateUniform4f("color", color_inside);
		sh.updateUniform4mat("projection", projection);
	// sh2.updateUniform("color", {0.1, 0.5, 0.7, 1});
		sh2.updateUniform4f("color", color_border);
		sh2.updateUniform4mat("projection", projection);

		glBindVertexArray(vao);
		


	}

	mesh::mesh( const std::vector<float>& VertexBuff, const std::vector<float>& color_inside, const std::vector<float>& color_border){
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vertexBuff);
		glGenBuffers(1, &indicesBuff);
		

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuff);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)* sizeof(VertexBuff), &VertexBuff[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glVertexPointer(2, GL_FLOAT, 0, BUFFER_OFFSET(0));
		glEnableClientState(GL_VERTEX_ARRAY);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuff);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(float)* sizeof(&indices), &indices, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, 0);

		glm::mat4 projection = glm::ortho(0.0f, (float)Application::get_width(), 0.0f, (float)Application::get_height(), -1.0f, 1.0f);
	// sh.updateUniform("aPos", {0.5, 0.5, 0.0, 1});
	// sh.updateUniform("color", {0.2, 0.2, 0.2, 1});
		sh.updateUniform4f("color", color_inside);
		sh.updateUniform4mat("projection", projection);
	// sh2.updateUniform("color", {0.1, 0.5, 0.7, 1});
		sh2.updateUniform4f("color", color_border);
		sh2.updateUniform4mat("projection", projection);

		glBindVertexArray(vao);

	}


	mesh::~mesh(){
		glDeleteBuffers(1, &vertexBuff);
		glDeleteBuffers(1, &indicesBuff);
		glDeleteBuffers(1, &colorBuff);

		glDeleteVertexArrays(1, &vao);
	}

	mesh mesh::operator=(const mesh& other){
		this->vao=other.vao;
		this->vertexBuff=other.vertexBuff;
		this->indicesBuff=other.indicesBuff;
		this->colorBuff=other.colorBuff;

		return *this;
	}

	void mesh::bindVao(){
		glBindVertexArray(vao);
	}
	void mesh::unbindVao(){
		glBindVertexArray(0);
	}

	const void mesh::fill_inside() const{
		sh.useProgram();
	}
	const void mesh::fill_border() const{
		sh2.useProgram();
	}

}