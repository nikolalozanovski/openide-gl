#include <primitives/vertex.h>
#include <iostream>
// std::vector<float> vertex::vertices;
vertex::vertex(const float& x,const float& y,const float& z){
  this->X = x;
  this->Y = y;
  this->Z = z;

  data.reserve(3);
  data.emplace_back(x);
  data.emplace_back(y);
  data.emplace_back(z);

std::cout<<"created vertex at: X: "<<x<<" Y: "<<y<<" Z: "<<z<<std::endl;

}



vertex::vertex(const vertex& v){
  this->X=v.X;
  this->Y=v.Y;
  this->Z=v.Z;
}

vertex::vertex(vertex& v){
  this->X=v.X;
  this->Y=v.Y;
  this->Z=v.Z;
}
vertex::vertex(const vertex&& v){
  this->X=v.X;
  this->Y=v.Y;
  this->Z=v.Z;
}

vertex::~vertex(){
  data.clear();
}
 float vertex::getX() const{
  return X;
 }
 float vertex::getY() const{
  return Y;
 }
 float vertex::getZ() const{
  return Z;
 }

 std::vector<float>& vertex::asVector(){
  return data;
}


