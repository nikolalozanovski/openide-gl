#include <render/renderable.h>
#include <iostream>
namespace renderables{
	 std::vector<float> renderable::vertices;
	 std::vector<std::unique_ptr<renderable>> renderable::elements;

 void renderable::insert(std::initializer_list<float > list){
vertices.insert(vertices.end(),list);
}
 void renderable::clear(){
vertices.clear();
}
const float& renderable::reference() noexcept{
	return renderable::vertices[0];
}
 const std::vector<float>& renderable::referencev() noexcept{
return renderable::vertices;
 }

const void renderable::insert_renderable_object(renderable* r){
	renderable::elements.emplace_back(std::unique_ptr<renderable>(std::move(r)));
}

// const std::vector<std::unique_ptr<renderable>>& renderable::get_all_elements() noexcept{
// 	return renderable::elements;
// }


void renderable::initiate_render(){
	for(auto&& t : renderable::elements){
		t->render();
	}
}

}