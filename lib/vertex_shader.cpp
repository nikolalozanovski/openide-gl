#include <shading/vertex_shader.h>


#include <iostream>
vertex_shader::vertex_shader(){
	
	// _vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	// std::string asd = __construct_shader();
	// const char *test = asd.c_str();

	// glShaderSource(_vertex_shader, 1, &test, NULL);
	// glCompileShader(_vertex_shader);
	GLenum err;
	_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::cout<<"vertex shader created"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	std::string asd = __construct_text_shader();
	const char *test = asd.c_str();

	glShaderSource(_vertex_shader, 1, &test, NULL);
	std::cout<<"vertex source specified"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	glCompileShader(_vertex_shader);
	std::cout<<"vertex shader compiled"<<std::endl;

	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;

	}
}
vertex_shader::vertex_shader(std::string& customShader){
	_vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	std::string asd = __construct_shader(customShader);
	const char *test = asd.c_str();

	glShaderSource(_vertex_shader, 1, &test, NULL);
	glCompileShader(_vertex_shader);

}
vertex_shader::~vertex_shader(){

}

const std::string vertex_shader::__construct_shader() const{
	return 
	"#version "+ std::to_string(version()) +"\n \
	attribute vec4 vertex; \n \
	uniform mat4 projection;\n \
	void main() \n \
	{ \n \
		gl_Position = projection * vec4(vertex.xy, 0.0, 1.0); \n \
	}";

}
/*

da se promeni shader za text

*/
const std::string vertex_shader::__construct_shader(std::string& customShader) const {
	return 
	"#version "+ std::to_string(version()) +" \n "+customShader;
}

const int vertex_shader::program(){
	return _vertex_shader;
}

const std::string vertex_shader::get_type(){
	return "VERTEX_SHADER";
}

const int vertex_shader::version() const{
	return 120;
}

const std::string vertex_shader::__construct_text_shader() const {
	// return 
	// "#version "+ std::to_string(version()) +"\n \
	// attribute vec4 vertex;\n \
	// varying vec2 TexCoords;\n \
	// uniform mat4 projection; \n \
	// void main() \n \
	// { \n \
	// 	gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);\n \
	// 	TexCoords = vertex.zw; \n \
	// }";
	return
	"#version "+ std::to_string(version()) +"\n \
	attribute vec4 vertex;\n \
	varying vec2 TexCoords;\n \
	uniform mat4 projection;\n \
	void main()\n \
	{\n \
		gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);\n \
		TexCoords = vertex.zw;\n \
	}";

}
const std::string vertex_shader::__construct_text_shader(std::string& customShader) const{
	return customShader;
}
