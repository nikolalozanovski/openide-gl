#include <shading/fragment_shader.h>
#include <iostream>
GLenum err;
fragment_shader::fragment_shader(){
	// _fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	// std::string asd = __construct_shader();
	// const char *test = asd.c_str();

	// int size = __construct_shader().length();
	// glShaderSource(_fragment_shader, 1, &test, &size);
	// glCompileShader(_fragment_shader);
	_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::cout<<"fragment shader created"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	std::string asd = text_shader();
	const char *test = asd.c_str();

	int size = text_shader().length();
	glShaderSource(_fragment_shader, 1, &test, &size);
	std::cout<<"fragment source specified"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	glCompileShader(_fragment_shader);
	std::cout<<"fragment shader compiled"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
}
fragment_shader fragment_shader::construct_button_shader() {

	std::string asd = button_shader();
	const char *test = asd.c_str();

	int size = button_shader().length();
	glShaderSource(_fragment_shader, 1, &test, &size);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	glCompileShader(_fragment_shader);
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	return *this;
}

fragment_shader fragment_shader::construct_text_shader() {
	_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::cout<<"text fragment shader created"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	std::string asd = text_shader();
	const char *test = asd.c_str();

	int size = text_shader().length();
	glShaderSource(_fragment_shader, 1, &test, &size);
	std::cout<<"fragment source specified"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}
	glCompileShader(_fragment_shader);
	std::cout<<"fragment shader compiled"<<std::endl;
	while((err = glGetError()) != GL_NO_ERROR)
	{
		std::cout<<gluErrorString(err)<<std::endl;
		std::cout<<err<<std::endl;
	}

	return *this;
}

void fragment_shader::operator=(const fragment_shader&){
	// *this = fragment_shader;
}


/*

da se promeni shader za text

*/





fragment_shader::fragment_shader(std::string& customShader){
	_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	std::string asd = button_shader(customShader);
	const char *test = asd.c_str();

	int size = button_shader().length();
	glShaderSource(_fragment_shader, 1, &test, &size);
	glCompileShader(_fragment_shader);

}
fragment_shader::~fragment_shader(){

}

std::string fragment_shader::button_shader()  {
	return 
	"#version "+ std::to_string(version()) +"\n \
	uniform vec4 color; \n \
	void main() \n \
	{ \n \
		gl_FragColor = color; \n \
	}";

}

const std::string fragment_shader::button_shader(std::string& customShader) const {
	return 
	"#version "+ std::to_string(version()) +" \n "+customShader;
}

const int fragment_shader::program(){
	return _fragment_shader;
}

const std::string fragment_shader::text_shader() const {
	return 
	"#version "+ std::to_string(version()) +"\n \
	varying vec2 TexCoords;\n \
	uniform sampler2D text;\n \
	uniform vec3 textColor;\n \
	void main()\n \
	{\n \
		vec4 sampled = vec4(1.0, 1.0, 1.0, texture2D(text, TexCoords).r);\n \
		gl_FragColor = vec4(textColor, 1.0) * sampled;\n \
	}";

}

const std::string fragment_shader::text_shader(std::string& customShader) const{
	return customShader;
}
// return 
	// "#version "+ std::to_string(version()) +"\n \
	// attribute vec2 TexCoords;\n \
	// varying vec4 color;\n \
	// attribute sampler2D text;\n \
	// uniform vec3 textColor;\n \
	// void main() \n \
	// { \n \
	// 	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);\n \
	// 	color = vec4(textColor, 1.0) * sampled;\n \
	// }";