#include <render/text.h>
#include <application.h>
#include <text/ft_char.h>

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// #include <glm/vec2.hpp>
// #include <glm/vec4.hpp>
// #include <glm/mat4x4.hpp>


namespace render{
	std::vector<text*> text::labels;
	void text::Render()  {
		for(auto t : text::labels){
			
			t->RenderText();
		}
	}

	text::text(std::string Characters, float X, float Y, float Scale, const std::vector<float>& color){

		characters = Characters;
		x = X;
		y = Y;
		scale = Scale;
		std::cout<<"size "<<color.size()<<std::endl; 
		if(color.size() == 3 || color.size() == 4 ){
			r = color[0];
			g = color[1];
			b = color[2];
			a = color[3];
		}else{
			r = 0.0;
			g = 0.0;
			b = 0.0;
		}

		// glEnable(GL_BLEND);
		// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, (sizeof(GLfloat) * 6 * 4), NULL, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);  


		labels.emplace_back(this);
		// RenderText(characters, x, y, scale, color); 
	}

	void text::RenderText(){
		// Activate corresponding render state	
		s.useProgram();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

		glm::mat4 projection = glm::ortho(0.0f, (float)Application::get_width(), 0.0f, (float)Application::get_height(), -1.0f, 1.0f);
		// std::cout<<x<<" "<<y<<std::endl;
		s.updateUniform3f("textColor",{r, g, b});
		s.updateUniform4mat("projection", projection);
	
		glActiveTexture(GL_TEXTURE0);
		glBindVertexArray(VAO);
	
		float temp_x {x};

		for(char c : characters){
			
			::text::ft_char ch = ::text::ft_char::get_character(int(c));

			glm::ivec2 bear = ch.get_bearing();
			glm::ivec2 size = ch.get_size();

			GLfloat xpos = x + bear.x * scale;
			GLfloat ypos = y - (size.y - bear.y) * scale;

			GLfloat w = size.x * scale;
			GLfloat h = size.y * scale;
        // Update VBO for each character
			GLfloat vertices[6][4] = {
				{ xpos,     ypos + h,   0.0, 0.0 },            
				{ xpos,     ypos,       0.0, 1.0 },
				{ xpos + w, ypos,       1.0, 1.0 },

				{ xpos,     ypos + h,   0.0, 0.0 },
				{ xpos + w, ypos,       1.0, 1.0 },
				{ xpos + w, ypos + h,   1.0, 0.0 }           
			};
        // Render glyph texture over quad
			glBindTexture(GL_TEXTURE_2D, ch.get_texture());
        // Update content of VBO memory
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); //site se vo edno
        // Render quad
			glDrawArrays(GL_TRIANGLES, 0, 6);
        // Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
        x += (ch.get_advance() >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64)
        // buffer_position += sizeof(vertices);
    }
    x = temp_x;

 
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

namespace preloading{

	void preload_characters(){
		for(char i=0; i!=127; i++){
				// std::cout<<&i<<std::endl;
			::text::ft_char(Application::request_library(), i);
		}

				// ::text::ft_char(Application::request_library(), 'A');
				// ::text::ft_char(Application::request_library(), 'B');


	}
}
}