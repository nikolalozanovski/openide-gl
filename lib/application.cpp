#include <application.h>
#include <iostream>

#include <render/text.h>

int Application::window;
int Application::w_height;
int Application::w_width;
text::freetype_lib Application::ft;

#ifdef __linux__
PFNGLXSWAPINTERVALEXTPROC Application::glXSwapIntervalEXT;
PFNGLXSWAPINTERVALMESAPROC Application::glXSwapIntervalMESA;
PFNGLXSWAPINTERVALSGIPROC Application::glXSwapIntervalSGI;
PFNGLXGETSWAPINTERVALMESAPROC Application::VsyncState;
#endif
int vsync;
void Application::gluMain(int argc, char* argv[]){
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 10);

    window=glutCreateWindow("hello");
    render::preloading::preload_characters();

    glClearColor(1.0, 1.0, 1.0, 1.0);

    glewInit();
    if(argv[1]== NULL){
        std::cout<<"Specify vertical sync state 1(YES)/0(NO) ";
        std::cin>>vsync;
        if(vsync >0){
            Application::VsyncOn(); 
        }else if(vsync==0){
            VsyncOff();
        }
        else if(vsync<0){
            std::cout<<"exiting application!"<<std::endl;
            exit(0);
        }
    }else{
        vsync = atoi(argv[1]);
        if(vsync ==1){
            Application::VsyncOn();        
        }else{
           VsyncOff();
       }

       


   }


}
void Application::gluMainLoop(){
    glutMainLoop();
}


Application::~Application(){
    glutDestroyWindow(window);
// glutLeaveMainLoop();
}

#ifdef __linux__
void Application::VsyncOn(){
   Display *dpy = glXGetCurrentDisplay();
   if (Application::glXSwapIntervalEXT != NULL) {
    Application::glXSwapIntervalEXT(dpy, 0, 1);
    std::cout<<"vertical sync turned on"<<std::endl;
} else {
    Application::glXSwapIntervalMESA = (PFNGLXSWAPINTERVALMESAPROC)glXGetProcAddress( (const GLubyte*)"glXSwapIntervalMESA");
    if ( Application::glXSwapIntervalMESA != NULL ) {
        Application::glXSwapIntervalMESA(1);
        std::cout<<"vertical sync turned on"<<std::endl;
    } else {
        Application::glXSwapIntervalSGI = (PFNGLXSWAPINTERVALSGIPROC)glXGetProcAddress( (const GLubyte*)"glXSwapIntervalSGI");
        if ( Application::glXSwapIntervalSGI != NULL ) {
            Application::glXSwapIntervalSGI(1);
            std::cout<<"vertical sync turned on"<<std::endl;    
        }
    }
}
}

void Application::VsyncOff(){
   Display *dpy = glXGetCurrentDisplay();
   if (Application::glXSwapIntervalEXT != NULL) {
    Application::glXSwapIntervalEXT(dpy, 0, 0);
    std::cout<<"vertical sync turned off"<<std::endl;
} else {
    Application::glXSwapIntervalMESA = (PFNGLXSWAPINTERVALMESAPROC)glXGetProcAddress( (const GLubyte*)"glXSwapIntervalMESA");
    if ( Application::glXSwapIntervalMESA != NULL ) {
        Application::glXSwapIntervalMESA(0);
        std::cout<<"vertical sync turned off"<<std::endl;
    } else {
        Application::glXSwapIntervalSGI = (PFNGLXSWAPINTERVALSGIPROC)glXGetProcAddress( (const GLubyte*)"glXSwapIntervalSGI");
        if ( Application::glXSwapIntervalSGI != NULL ) {
            Application::glXSwapIntervalSGI(0);
            std::cout<<"vertical sync turned off"<<std::endl;    
        }
    }
}
}
#endif

text::freetype_lib Application::request_library(){
    return ft.get_current_object();
}

void Application::window_dimensions(int w, int h){
  w_height = h;
  w_width = w;

}
int Application::get_height(){
    return w_height;
}
int Application::get_width(){
    return w_width;
}
