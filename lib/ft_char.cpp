#include <text/ft_char.h>
#include <iostream>

#include <glinc.h>

namespace text{

	std::vector<ft_char> ft_char::characters;

	ft_char::ft_char(freetype_lib lib, char character){
		if (FT_Load_Char(lib.get_face(), character, FT_LOAD_RENDER)){
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;  
		}else{
			std::cout<<"character: <<"<<character<<">> successfully loaded!"<<std::endl;
			
		}
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			lib.get_face()->glyph->bitmap.width,
			lib.get_face()->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			lib.get_face()->glyph->bitmap.buffer
			);
    // Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		texture_id = texture;
		advance = lib.get_face()->glyph->advance.x;
		size = glm::ivec2(lib.get_face()->glyph->bitmap.width, lib.get_face()->glyph->bitmap.rows);
		bearing = glm::ivec2(lib.get_face()->glyph->bitmap_left, lib.get_face()->glyph->bitmap_top);
		
	

		characters.emplace_back(std::move(*this));
	}

	int& ft_char::get_texture() noexcept{
		return texture_id;
	}
	int& ft_char::get_advance() noexcept{
		return advance;
	}
	glm::ivec2& ft_char::get_size() noexcept{
		return size;
	}
	glm::ivec2& ft_char::get_bearing() noexcept{
		return bearing;
	}
	ft_char ft_char::get_character(int pos){
		return ft_char::characters.at(pos);
	}
}