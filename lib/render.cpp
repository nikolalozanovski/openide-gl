#include <render/render.h>
// #include <primitives/shapes.h>

#include <render/renderable.h>
#include <render/text.h>
#include <application.h>
#include <cmath>
#include <string>
class vertex;
#include <iostream>

namespace rendering{

	float arandom(){

		return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	}


	int frame = 0,
	time =0,
	timebase = 0,
	fps=0;


	void render::Render(){

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1.0, 1.0, 1.0, 1.0);

// glLineWidth(10.0);
		renderables::renderable::initiate_render();

		// ::render::text(std::to_string(fps), 200, 200, 1.0, {0.3, 0.5, 0.7});
		::render::text::Render();

		frame++;

		time=glutGet(GLUT_ELAPSED_TIME);

		if (time - timebase > 1000) {
			fps = frame*1000.0/(time-timebase);
			timebase = time;
			frame = 0;

		}
		std::cout<<fps<<std::endl;
glutSwapBuffers();   // swapping image buffer for double buffering

// std::cout<<fps<<std::endl;

}


void render::Reshape(int w, int h){
	glViewport(0, 0, w, h);
	Application::window_dimensions(w,h);

    // glMatrixMode(GL_PROJECTION);
    // glLoadIdentity();
    // glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);

    // glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();
	glutPostRedisplay(); 
}


void render::Idle(){
	
	glutPostRedisplay(); 

}
}