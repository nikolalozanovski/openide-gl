
#include <mesh/button.h>


#ifndef BUFF_OFFSET
#define BUFF_OFFSET
#define BUFFER_OFFSET(bytes)((GLubyte*) NULL + (bytes))
#endif

namespace mesh{
	button::button(const float& x, const float& y,const float& height ,const float& width,
		const std::vector<float>& color_inside, const std::vector<float>& color_border){
		primitives::p2d::line2d (vertex(x,y), vertex(x+width, y));
		primitives::p2d::line2d (vertex(x+width, y), vertex(x+width, y-height));
		primitives::p2d::line2d (vertex(x+width, y-height), vertex(x, y-height));
		primitives::p2d::line2d (vertex(x, y-height), vertex(x, y));


		renderables::renderable::insert({x,y,x+width,y,x+width,y+height,x,y+height});
		m.prepare(renderables::renderable::referencev(), color_inside, color_border);
		renderables::renderable::clear();
		

		X = x;
		Y = y;

		Width = width;
		Height = height;

		renderables::renderable::insert_renderable_object(std::move(this));
		
	}


	
	void button::render(){
		m.bindVao();
		
		m.fill_inside();
		glDrawElements(GL_TRIANGLE_STRIP,6, GL_UNSIGNED_INT, BUFFER_OFFSET(0));
		
		m.fill_border();
		glDrawArrays(GL_LINE_LOOP,0,4);;
	}
}