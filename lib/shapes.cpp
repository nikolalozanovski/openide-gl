#include <primitives/shapes.h>
#include <iostream>

shapes::~shapes(){
	data.clear();
}
bool shapes::emplace(float Data){
	data.emplace_back(Data);
	return true;
}

bool shapes::reserve_data(size_t count){
	
	size_t current_data = data.size();
	data.reserve(count);
	if(current_data+count != data.size()){
		return false;
	}
	return true;
}
