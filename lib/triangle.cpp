#include <primitives/triangle.h>

namespace primitives{
	namespace p2d{
		triangle2d::triangle2d(const vertex& v1, const vertex& v2, const vertex& v3){
			data.reserve(9);
			data.emplace_back(std::move(v1.getX()));
			data.emplace_back(std::move(v1.getY()));
			data.emplace_back(0.0);

			data.emplace_back(std::move(v2.getX()));
			data.emplace_back(std::move(v2.getY()));
			data.emplace_back(std::move(0.0));


			data.emplace_back(std::move(v3.getX()));
			data.emplace_back(std::move(v3.getY()));
			data.emplace_back(0.0);

		}

		triangle2d::triangle2d(const line2d& v1,const line2d& v2, const line2d& v3){
			data.reserve(9);
			auto v = v1.this_vertices();
			data.emplace_back(v[0].getX());
			data.emplace_back(v[0].getY());
			data.emplace_back(0.0);
			data.emplace_back(v[1].getX());
			data.emplace_back(v[1].getY());
			data.emplace_back(0.0);
			v = v2.this_vertices();
			data.emplace_back(v[1].getX());
			data.emplace_back(v[1].getY());
			data.emplace_back(0.0);
		}

		triangle2d::triangle2d(const line2d& v1,const line2d& v2){
			data.reserve(9);
			auto v = v1.this_vertices();
			data.emplace_back(v[0].getX());
			data.emplace_back(v[0].getY());
			data.emplace_back(0.0);
			data.emplace_back(v[1].getX());
			data.emplace_back(v[1].getY());
			data.emplace_back(0.0);
			v = v2.this_vertices();
			data.emplace_back(v[1].getX());
			data.emplace_back(v[1].getY());
			data.emplace_back(0.0);
		}

		const std::string triangle2d::getType(){
			return "2DTRIANGLE";
		}
		int triangle2d::getlength(){

			return data.size();
		}
	}
}