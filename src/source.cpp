#include <iostream>
#include <render/render.h>
// #include <render/text.h>
#include <application.h>
#include <primitives/polygon.h>


#include <stdlib.h>     /* srand, rand */
#include <time.h> 

#include <mesh/button.h>
#include <shading/colors.h>

float arandom(){
	
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

int main(int argc, char* argv[]){
	srand(time(0));
	Application::gluMain(argc, argv);
	Application::gluAddDisplayFunc(rendering::render::Render);
	Application::gluAddReshapeFunc(rendering::render::Reshape);
	// Application::gluAddIdleFunc(rendering::render::Idle);

	Application::window_dimensions(800,600);
	// primitives::p2d::polygon2d ooo ({vertex(0.5,0.0,0.0),vertex(0.5,0.0,0.0),vertex(0.5,0.0,0.0)});


	
	// new mesh::button(-0.9, -0.9, 1.8, 1.8, coloring::color_table::get_color(color::silver), coloring::color_table::get_color(color::lime));
	// new mesh::button(100, 100, 200, 200, coloring::color_table::get_color(color::silver), coloring::color_table::get_color(color::lime));
	// new render::text("render something", 120, 200, 1.0, coloring::color_table::get_color(color::black));


	std::cout<<"entering main loop"<<std::endl;
	Application::gluMainLoop();
}


