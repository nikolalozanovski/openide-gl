
#include <glinc.h>
#ifndef FREETYPE_TEXT
#define FREETYPE_TEXT
#include <text/ft_class.h>
#endif

#ifndef FUNCTIONAL
#define FUNCTIONAL
#include <functional>
#endif

class Application{
public:
	
	static void gluMain(int argc, char* argv[]);

//template<typename function>
//static void gluAddDisplayFunc(function displayFunc);

template<typename function>
	static void gluAddDisplayFunc(const function displayFunc){
		glutDisplayFunc(displayFunc);
	}

template<typename function>
	static void gluAddReshapeFunc(const function reshapeFunc){
		glutReshapeFunc(reshapeFunc);
	}
template<typename function>
	static void gluAddIdleFunc(const function idleFunc){
		glutIdleFunc(idleFunc);
	}
	static void gluMainLoop();
	

	static void VsyncOn();
	static void VsyncOff();

	static void window_dimensions(int w, int h);
	static int get_height();
	static int get_width();
	static text::freetype_lib request_library();


	Application() = default;
	~Application();


private:
	static int window;

	static int w_height;
	static int w_width;

	static text::freetype_lib ft;


	#ifdef __linux__
	static PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT;
	static PFNGLXSWAPINTERVALMESAPROC glXSwapIntervalMESA;
	static PFNGLXSWAPINTERVALSGIPROC glXSwapIntervalSGI;
	static PFNGLXGETSWAPINTERVALMESAPROC VsyncState;
    #endif
};