#ifndef GLRender
#define GLRender
#ifdef __linux__
#define GL_GLEXT_PROTOTYPES
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <X11/Xlib.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <GL/glxext.h>

#endif



#ifdef _WIN32
#define GL_GLEXT_PROTOTYPES
#include "windows.h"
#include <GL/gl.h>
#include <GL/glu.h>
#define GLUT_DISABLE_ATEXIT_HACK
#include <GL/glut.h>
#endif
#endif