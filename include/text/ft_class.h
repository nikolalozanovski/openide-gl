#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#ifndef FT_LIBRARY_INCLUDE
#define FT_LIBRARY_INCLUDE
#include <ft2build.h>
#include FT_FREETYPE_H  
#endif

namespace text{
	
	class freetype_lib{
	public:
		freetype_lib();
		~freetype_lib();

		std::vector<int> version() const;
		bool compare_version(const int& major, const int& minor, const int& patch) noexcept;

		FT_Library get_library();
		FT_Face get_face();
		freetype_lib get_current_object();

	private:
		int major= -1;
		int minor= -1;
		int patch= -1;

		FT_Library  library;
		FT_Face face;
	};
}