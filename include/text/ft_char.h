
#ifndef FREETYPE_TEXT
#define FREETYPE_TEXT
#include <text/ft_class.h>
#endif

#ifndef GLM_VEC2
#define GLM_VEC2
#include <glm/vec2.hpp>
#endif


#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#include <iostream>

namespace text{
	class ft_char{
	public:
		ft_char(freetype_lib lib, char character);
		~ft_char() = default;

		int& get_texture() noexcept;
		int& get_advance() noexcept;
		glm::ivec2& get_size() noexcept;
		glm::ivec2& get_bearing() noexcept;

		static ft_char get_character(int at);

		
	private:
		unsigned int texture;



		int texture_id;
		int advance;
		glm::ivec2 size;
		glm::ivec2 bearing;


		
		static std::vector<ft_char> characters;
	};
}
