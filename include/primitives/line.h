/*
 * These classes represent one line or commonly known as vector in graphics library.
 * Each vector contains 2 vertices.
 * Line(vector) is derived from shapes which contain an standard library vector of data. Data is handled by
 * base class shapes.
 * Lines can be in 2d or 3d space 
 */

#ifndef SHAPES_h
#define SHAPES_h
#include "shapes.h"
class shapes;
#endif


/*
  this namespace contain data from primitives and are stored in the main(RAM) memory
  every primitive object is subclass of the class 'shapes'
*/
namespace primitives{
/*
  primitive 2d is the full name of the namespace
*/
  namespace p2d{
    /*
    this class represent one primitive instance of a line(vector in CGI terms)
    */
    class line2d : public shapes{
    public:
      /*
      returns what is this object
      */
      const std::string getType();
      /*
      constructs and returns vertices from the data that this line contain
      */
      std::vector<vertex> this_vertices() const;

      /*
      RAII
      */
      line2d(float x1, float y1, float x2, float y2);
      line2d(const vertex& v1,const vertex& v2);

      line2d(line2d&& a) = default;
      ~line2d() = default;
      line2d(line2d& a) = default;

    private:

    };
  }

/*
#ifndef LINE_3D
#define LINE_3D
namespace dd{
class line3d : public shapes{
 public:
  line3d(const vertex& v1,const vertex& v2);
 private:
  line3d(const line3d& a)=delete;
  line3d(const line3d&& a)=delete;
};
}
#endif
*/
}

