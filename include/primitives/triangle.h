#ifndef VERTEX
#define VERTEX
#include <primitives/vertex.h>
#endif

#ifndef LINE
#define LINE
#include <primitives/line.h>
#endif

#ifndef SHAPES_h
#define SHAPES_h
#include "shapes.h"
class shapes;
#endif
namespace primitives{
	namespace p2d{
		class triangle2d : public shapes{
		public:
			const std::string getType();
			triangle2d(const vertex& v1,const vertex& v2, const vertex& v3);
			triangle2d(const line2d& v1,const line2d& v2, const line2d& v3);

			//optimised version of construction, does not require redundant data when
			//using lines to construct the triangle
			triangle2d(const line2d& v1,const line2d& v2);
			triangle2d( triangle2d& t) = default;		
			triangle2d( triangle2d&& t) = default;
			int getlength();
		private:

		
		};
	}
/*
class triangle3d : public shapes{
 public:
triangle2d(const vertex& v1,const vertex& v2);
 private:
triangle2d(const triangle2d& t);
triangle2d(const triangle2d&& t);
};
*/
}