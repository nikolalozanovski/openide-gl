/* This class represent one vertex in cartesian space
 * each vertex has 3 values for each position
 * for 3d space all 3 fields are used
 * for 2d first 2 (x,y) but,
 * third field is ommitted
 */

#ifndef STRING
#define STRING
#include <string>
#endif

#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif


class vertex{
 public:
  vertex(const float& x,const float& y,const float& z=0);
  vertex(const vertex&& a);
  vertex(const vertex& a);
  vertex(vertex& a);

   ~vertex();

  inline const std::string getType(){
  	return "VERTEX";
  }

 float getX() const;
 float getY() const;
 float getZ() const;

 std::vector<float>& asVector();

 private:

  std::vector<float> data;
  float X = 0.0; // position in 'X' cordinate
  float Y = 0.0; // position 'Y' cordinate
  float Z = 0.0; // position in 'Z'
  vertex();
 
};
