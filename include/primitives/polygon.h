
#ifndef VERTEX
#define VERTEX
#include "vertex.h"
class vertex;
#endif

#ifndef LINE
#define LINE
#include <primitives/line.h>
#endif

#ifndef TRIANGLE
#define TRIANGLE
#include <primitives/triangle.h>
#endif

#ifndef SHAPES_h
#define SHAPES_h
#include "shapes.h"
class shapes;
#endif

#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif
namespace primitives{
	namespace p2d{

		class polygon2d : public shapes{
		public:
			const std::string getType(){
				return "2DPOLYGON";
			}
			polygon2d() = default;
			polygon2d(const std::vector<vertex>& v);
			polygon2d(const std::vector<line2d>& v);
			polygon2d(const std::vector<triangle2d>& v);

			// polygon2d(const line2d l1, const vertex v1);
			// polygon2d(const vertex v1, const line2d l1);

			polygon2d( polygon2d& t) = default;
			polygon2d( polygon2d&& t) = default;
			~polygon2d() = default;
			int getlength();
			
		private:


		};
	}
}