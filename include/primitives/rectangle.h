
#ifndef VERTEX
#define VERTEX
#include <primitives/vertex.h>
#endif
#ifndef LINE
#define LINE
#include <primitives/line.h>
#endif

#ifndef SHAPES_h
#define SHAPES_h
#include "shapes.h"
class shapes;
#endif

namespace primitives{
	namespace p2d{
		class rect2d : public shapes{
		public:
			const std::string getType();
			rect2d(const vertex& v1,const vertex& v2, const vertex& v3,const vertex& v4);
			rect2d(const line2d& l1,const line2d& l2, const line2d& l3, const line2d& l4);

			//optimised version of construction, does not require redundant data when
			//using lines to construct the rectangle
			rect2d(const line2d& l1,const line2d& l2);

			rect2d( rect2d& t) = default;
			rect2d( rect2d&& t) = default;
			
		private:


		};
	}
}