/*
 * Shapes class is used as base class for all shapes that have vertices.
 * Data represented in all derived class of shapes are handled by this class
*/
#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#ifndef VERTEX
#define VERTEX
#include <primitives/vertex.h>
#endif


#ifndef STRING
#define STRING
#include <string>
#endif

#ifndef MEMORY_h
#define MEMORY_h
#include <memory>
#endif


class shapes{
public:
  /*
  every subclass must implement this function. The reason why
  is because engine must know which type is its subclass
  --not implemented--
  */
  virtual const std::string getType()=0;

  shapes()=default;

  shapes(shapes& s){
   std::move(s);
 }
 shapes(shapes&& s){
   std::move(s);
 }
 virtual ~shapes();

protected:
  /*
every subclass must provide it's own method of getting the data,
subclass must wrap this function in order to get underlined data
  */
 std::vector<float>& getData();
 /*
  further optimise vector insertion by reserving storage for n('count') elements
 */
 bool reserve_data(size_t count);
 /*
  emplace one float to the data storage
 */
 bool emplace(float data);

/*
  to be changes as private member
*/
 std::vector<float> data;
private:



};
