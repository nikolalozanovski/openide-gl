#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#ifndef INITLIST
#define INITLIST
#include <initializer_list>
#endif		

#ifndef MEMORY
#define MEMORY
#include <memory>
#endif

#ifndef RENDERABLE 
#define RENDERABLE

namespace renderables{

class renderable{
public:
// static const std::vector<std::unique_ptr<renderable>>& get_all_elements() noexcept;
virtual void render()=0;
virtual ~renderable()=default ;
renderable( renderable&) = default ;
renderable( renderable&&) = default ;
static void initiate_render();
protected:
	renderable() = default;
	
	

	
	static void insert(std::initializer_list<float>);
	static void clear();
	static const float& reference() noexcept;
	static const std::vector<float>& referencev() noexcept;
	
	static const void insert_renderable_object(renderable* );

private:
	
// 	  __attribute__ ((destructor)) static void clear_elements(){
// 	for(auto it = elements.begin(); it != elements.end(); ++it){
//   	delete *it;}
// }

	static std::vector<float> vertices;
	static std::vector<std::unique_ptr<renderable>> elements;


};

}
#endif