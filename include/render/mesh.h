
#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#ifndef MESH
#define MESH
#include <glinc.h>

#ifndef SHADER
#define SHADER
#include <shading/shader.h>
#endif

namespace render{

	class mesh{
	public:
		mesh()=default;
		mesh(const std::vector<float>& VertexBuff, const std::vector<float>& color_inside, const std::vector<float>& color_border);
		const void prepare(const std::vector<float>& VertexBuff, const std::vector<float>& color_inside, const std::vector<float>& color_border);

		~mesh();

		mesh operator=(const mesh&);

		void bindVao();
		void unbindVao();

		const void fill_inside() const;
		const void fill_border() const;

	protected:



	private:
		
		unsigned int vao;
		unsigned int vertexBuff;
		unsigned int indicesBuff;
		unsigned int colorBuff;



// int colorBuff;

// int textureBuff;


		shader sh = shader(shader::type::button);
		shader sh2 = shader(shader::type::button);
		// shader sh3 = shader(shader::type::button);
	};
}
#endif