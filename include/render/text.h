
#ifndef STRING
#define STRING
#include <string>
#endif


#ifndef SHADER
#define SHADER
#include <shading/shader.h>
#endif

#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

namespace render{
	
	class text{
	public:
		text() = default;
		~text() = default;

		text(std::string characters, float x, float y, float scale,const std::vector<float>& color);

		void RenderText();

		static void Render()  ;
	protected:
	private:

		shader s = shader(shader::type::text);

		std::string characters;
		unsigned int VAO;
		unsigned int VBO;

		
		float x;
		float y;
		float scale;
		float r;
		float g;
		float b;
		float a;


		static std::vector<text*> labels;
	};
	


	namespace preloading{
		void preload_characters();

	}
}