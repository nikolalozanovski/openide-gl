#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif


#include <render/renderable.h>
#include <render/mesh.h>


#ifndef LINE
#define LINE
#include <primitives/line.h>
#endif

namespace mesh{

	class button : renderables::renderable{
	public:
		button(const float& x, const float& y,const float& height ,const float& width, 
			const std::vector<float>& color_inside, const std::vector<float>& color_border);
		~button() = default;
	protected:

	private:
		void render () final;

		render::mesh m;
		float X;
		float Y;
		float Height;
		float Width;

	};

}
