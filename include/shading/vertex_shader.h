#include <shading/shader_base.h>

#ifndef VERTEX_SHADER
#define VERTEX_SHADER

class vertex_shader : private shader_base{
public:
	 const std::string get_type();
	 const int version() const;

	vertex_shader();
	vertex_shader(std::string& customShader);
	~vertex_shader();

	vertex_shader(vertex_shader&) = default;
	vertex_shader(vertex_shader&&) = default;

	const std::string __construct_shader() const ;
	const std::string __construct_shader(std::string& customShader) const ;

	const std::string __construct_text_shader() const ;
	const std::string __construct_text_shader(std::string& customShader) const ;



	const int program();

	
protected:
private:
	unsigned int _vertex_shader;
};
class vertex_shader;
#endif