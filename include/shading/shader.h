#include <shading/fragment_shader.h>
#include <shading/vertex_shader.h>


#ifndef VECTOR_h
#define VECTOR_h
#include <vector>
#endif

#ifndef GLM
#define GLM
#include <glm/glm.hpp>
#endif

#ifndef SHADER_COMPOSITION
#define SHADER_COMPOSITION
class shader{
public:

	enum type{
		error = -1,
		button = 0,
		text = 1
	};

	shader();
	shader(enum shader::type t);
	~shader() = default;
	shader(shader&) = default;
	shader(shader&&) = default;


	const void updateUniform3f(std::string uniform_name, const std::vector<float>& uniform_data) const;
	const void updateUniform4f(std::string uniform_name, const std::vector<float>& uniform_data) const;
	const void updateUniform4mat(std::string uniform_name, glm::mat4 matrix) const;
	const void updateUniform1i(std::string uniform_name, int data) const;
	const void bindLocation(std::string attribute, int location) const;
	const void useProgram() const;

	const unsigned int getProgram() const;

protected:
private:
	vertex_shader vs;
	fragment_shader fs;
	
	unsigned int shaderProgram;
};
#endif