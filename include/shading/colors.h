#ifndef init_list
#define init_list
#include <initializer_list>
#endif

namespace coloring{


#ifndef COLOR_TABLE
#define COLOR_TABLE


class color_table{
public:
color_table() = default;
~color_table() = default;

 enum class prebuilt_list : int{
		white = 1,
		black,
		gray,
		red,
		green,
		blue,
		cyan,
		magenta,
		silver,
		maroon,
		olive,
		lime,
		purple,
		teal,
		navy
	};
const static  std::initializer_list<float> get_color(const prebuilt_list color){
	switch(color){
		case prebuilt_list::white:
		return {1.0, 1.0, 1.0, ALPHA};
		break;
		case prebuilt_list::black:
		return {0.0, 0.0, 0.0, ALPHA};
		break;
		case prebuilt_list::gray:
		return {0.5, 0.5, 0.5, ALPHA};
		break;

		case prebuilt_list::red:
		return {1.0, 0.0, 0.0, ALPHA};
		break;

		case prebuilt_list::green:
		return {0.0, 0.5, 0.0, ALPHA};
		break;

		case prebuilt_list::blue:
		return {0.0, 0.0, 1.0, ALPHA};
		break;

		case prebuilt_list::cyan:
		return {0.0, 1.0, 1.0, ALPHA};
		break;

		case prebuilt_list::magenta:
		return {1.0, 0.0, 1.0, ALPHA};
		break;

		case prebuilt_list::silver:
		return {0.75, 0.75, 0.75, ALPHA};
		break;

		case prebuilt_list::maroon:
		return {0.5, 0.0, 0.0, ALPHA};
		break;

		case prebuilt_list::olive:
		return {0.5, 0.5, 0.0, ALPHA};
		break;

		case prebuilt_list::lime:
		return {0.0, 1.0, 0.0, ALPHA};
		break;

		case prebuilt_list::purple:
		return {0.5, 0.0, 0.5, ALPHA};
		break;

		case prebuilt_list::teal:
		return {0.0, 0.5, 0.5, ALPHA};
		break;

		case prebuilt_list::navy:
		return {0.0, 0.0, 0.5, ALPHA};
		break;

		default:
		return {0.0, 0.0, 0.0, 0.0};

	}
}


protected:
private:
	static const int ALPHA = 1.0;
};
class color_table;
}
using color = coloring::color_table::prebuilt_list;
using c_t = coloring::color_table;
#endif