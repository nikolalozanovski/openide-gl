#include <glinc.h>

#ifndef STRING
#define STRING
#include <string>
#endif

#ifndef SHADER_BASE
#define SHADER_BASE
class shader_base{
public:

	shader_base() = default;
	virtual ~shader_base() = default;

	shader_base(shader_base&) = default;
	shader_base(shader_base&&) = default;

virtual const std::string get_type () =0;
virtual const int version() const =0;
virtual const int program() =0;
protected:
private:

};
class shader_base;
#endif