#include <shading/shader_base.h>

#ifndef FRAGMENT_SHADER
#define FRAGMENT_SHADER

class fragment_shader : private shader_base{
public:
	const std::string get_type() {
		return "FRAGMENT_SHADER";
	}
	const int version() const{
		return 120;
	}

	enum shader_type{
		button =0,
		text =1
	};

	fragment_shader();
	fragment_shader(std::string& customShader);
	~fragment_shader();

	fragment_shader(fragment_shader&) = default;
	fragment_shader(fragment_shader&&) = default;

	std::string button_shader();
	const std::string button_shader(std::string& customShader) const ;

	const std::string text_shader() const ;
	const std::string text_shader(std::string& customShader) const ;

	const int program();
	

	fragment_shader construct_button_shader() ;
	fragment_shader construct_text_shader() ;

	void operator=(const fragment_shader&);


protected:
private:
	unsigned int _fragment_shader;
};
class fragment_shader;
#endif